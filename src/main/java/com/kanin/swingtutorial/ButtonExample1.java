package com.kanin.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import com.vaadin.client.ui.ImageIcon;

public class ButtonExample1 {
    JButton button;
    JButton clearButton;
    JTextField text;
    public ButtonExample1() {
        super("Button Example");
        text = new JTextField();
        text.setBounds(50,50,150,20);
        JButton button = new JButton("Welcome");
        button.setBounds(50,100,110,30);
        button.setIcon(ImageIcon("welcome.png"));
        button.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("Welcome to BU");
            }
        });
        clearButton = new JButton("Clear");
        clearButton.setBounds(165,100,95,30);
        clearButton.setIcon(new ImageIcon("clear.png"));
        clearButton.addActionListener(new ActionListener());
            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("");
            }
        });
        this.add(text);
        this.add(button);
        this.add(clearButton);
        this.setSize(400,400);
        this.setLayout(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        ButtonExample1 frame = new ButtonExample1();
    }
}
 