package com.kanin.swingtutorial;

import javax.swing.*;

public class HelloMyName extends JFrame{
    JLabel lblName;
    JTextField txtName;
    JButton btnHello;
    JLabel lblHello;
        public HelloMyName(){
        super("Hello My Name");
        lblName = new JLabel("name:");
        lblName.setBounds(10,10,100,20);
        lblName.setHorizontalAlignment(JLabel.RIGHT);;
        txtName = new JTextField();
        txtName.setBounds(70,10,200,20);
        btnHello = new JButton("Hello");
        btnHello.setBounds(30,20,250,20);

        lblHello = new JLabel("Hello My Name");
        lblHello.setHorizontalAlignment(JLabel.RIGHT);
        lblHello.setBounds(30,70,250,20);
        this.add(lblName);
        this.add(txtName);
        this.add(btnHello);
        this.add(lblHello);

        this.setLayout(null);
        this.setSize(400,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();
    }
}
