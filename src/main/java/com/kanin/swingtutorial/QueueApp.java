package com.kanin.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class QueueApp extends JFrame{
    JTextField txtName;
    JLabel lblQueueList,lblCurrent;
    JButton btnAddQueue,btnGetQueue,btnClearQueue;

    public QueueApp(){
        super("Queue App");
        this.setSize(400,300);
        txtName = new JTextField();

        this.add(txtName);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public static void main(String[] args) {
        
    }
}
